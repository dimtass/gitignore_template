Git .gitignore template file
----

Template .gitignore for arm/x86 Windows/Linux C/C++ development.
It also ignores Eclipse, Visual Studio, VS Code and sublime project files.

>For ARM development, .bin and .hex files are also ignored.

Before commit a new project *always* check that all the needed files are included and if not then remove the extension from .gitignore

In case you need to update this .gitignore, also update gitignore.txt